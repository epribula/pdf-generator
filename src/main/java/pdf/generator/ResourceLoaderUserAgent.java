package pdf.generator;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.xhtmlrenderer.pdf.ITextOutputDevice;
import org.xhtmlrenderer.pdf.ITextUserAgent;

/**
 * @author Jakub Pietrzak
 */
public class ResourceLoaderUserAgent extends ITextUserAgent {

    public ResourceLoaderUserAgent(ITextOutputDevice outputDevice) {
        super(outputDevice);
    }

    @Override
    protected InputStream openStream(String uri) throws FileNotFoundException {
        String resourcePath = Paths.get(getBaseURL(), uri).toString();

        InputStream stream;
        try {
            stream = new FileInputStream(resourcePath);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ResourceLoaderUserAgent.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }

        return stream;
    }

    @Override
    public String resolveURI(String uri) {
        return uri;
    }
}
