/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pdf.generator;

import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.xhtmlrenderer.pdf.ITextRenderer;

/**
 *
 * @author epribula
 */
public class App {

    public static ITextRenderer createRenderer() {
        ITextRenderer renderer = new ITextRenderer();

        ResourceLoaderUserAgent callback = new ResourceLoaderUserAgent(renderer.getOutputDevice());
        callback.setSharedContext(renderer.getSharedContext());
        renderer.getSharedContext().setUserAgentCallback(callback);

        return renderer;
    }

    public static void createPdf(String xhtmlFilePath, String templateFolderPath, String outputFilePath) throws Exception {

        ITextRenderer renderer = createRenderer();
        
        byte[] encoded = Files.readAllBytes(Paths.get(xhtmlFilePath));
        renderer.setDocumentFromString(new String(encoded, StandardCharsets.UTF_8), templateFolderPath);
        renderer.layout();

        try (OutputStream os = Files.newOutputStream(Paths.get(outputFilePath))) {
            renderer.createPDF(os);
        }
    }

    public static void main(String[] args) throws Exception {
        Options options = new Options();

        Option input = new Option("i", "input", true, "input file path");
        input.setRequired(false);
        options.addOption(input);

        Option output = new Option("o", "output", true, "output file");
        output.setRequired(false);
        options.addOption(output);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();

        CommandLine cmd;
        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("utility-name", options);
            throw e;
        }

        String inputFilePath = cmd.getOptionValue("input") == null ? "template.xhtml" : cmd.getOptionValue("input");
        String outputFilePath = cmd.getOptionValue("output") == null ? inputFilePath.replace(".xhtml", ".pdf") : cmd.getOptionValue("output");
        String templateFolderPath = Paths.get(inputFilePath).getParent().toString();
        
        System.out.println("Template folder: " + templateFolderPath);
        System.out.println("Input xhtml: " + inputFilePath);
        System.out.println("Output pdf: " + outputFilePath);
        
        createPdf(inputFilePath, templateFolderPath, outputFilePath);
        
        System.out.println("FINISHED SUCCESSFULLY");
    }
}
